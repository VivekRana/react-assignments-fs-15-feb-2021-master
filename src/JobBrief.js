import React, { Component } from 'react';

class JobBrief extends Component {
    render() {
        return (
            <div>
            <div className="jobs" style={{maxWidth: "20vw"}}>
              <h4>{this.props?.job?.name}</h4>
              <h5>{this.props?.job?.location?.city}</h5>
              <img src={this.props?.job?.logo}  className="job-post-image" />
              <p>
                {this.props?.job?.description}
              </p>
              <div className="salary">Salary:   {this.props?.job?.salary}</div>
              <hr/>
            </div>
            </div>
        );
    }
}

export default JobBrief;