import './App.css';
import Demo from './Demo';
import JobBrief from './JobBrief';
import JobBriefList from './JobBriefList';
import Jobs from "./jobs.json";

function App() {



  return (
    <div className="App">
      {/* <header className="App-header">
        <p>Assignment 1</p>
        <a 
          className="App-link"
          href="https://docs.google.com/document/d/1F9JXxwHegf-X0dxC_r51Q7hgD2YU8s8EQTG4HJvKG0g/edit?usp=sharing"
          target="_blank"
        >
          doc
        </a>
      </header> */}
      {/* <Demo/> */}
        <JobBriefList Jobs={Jobs}/>
    </div>
  );
}

export default App;
