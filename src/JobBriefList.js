import React, { Component } from 'react';
import JobBrief from './JobBrief';

class JobBriefList extends Component {

     listItems = this.props.Jobs.map((job) =>  
     <JobBrief job={job}/>
  );  

    render() {
        return (
            <div>
                 {this.listItems}
            </div>
        );
    }
}

export default JobBriefList;